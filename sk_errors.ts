/**
 * Enumeration of all errors sidekick can possibly produce
 *
 * Generally, sidekick tries to avoid being oopy, but conforming to
 * standard JavaScript error types is more important.
 *
 * @module
 */


abstract class SkError extends Error;


