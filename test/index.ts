/**
 * Top-level test suite for Jaeck Russell
 *
 * To start with, we are writing JR as a runtime debugger for sidekick. And
 * sidekick needs a rewrite basically to deal with the new safety monad idiom
 * (really to test to see if that's a thing that can work). So in doing this I
 * am gradually going to rewrite sidekick in the Jaeck Russell source tree, and
 * then pull it out into its own project.
 *
 * Starting here with just testing out/workshopping safem idioms
 *
 * What I am learning is that Haskell's monad idiom really heavily relies on
 * lazy-evaluation-by-default, and really what I need to do is develop an idiom
 * for controlling evaluation order.
 *
 * @module
 */

import      {ok, err, safe, unsafe} from '../src/sk_safem';
import type {Safe}                  from '../src/sk_safem';

// let's start with copying the example from the documentation


/**
 * gives back the number if the number is greater than or equal to 5
 *
 * otherwise errors
 */
function
ge5
    (n : number)
    : Safe<number, string>
{
    if (n >= 5)
        return ok(n);
    else
        return err(`${n} is less than 5`);
}


/**
 * Pure function that subtracts 11
 */
function
sub11
    (n : number)
    : number
{
    return n - 11;
}


/**
 * main function, runs everything
 */
function
main
    ()
    : void
{
    let foo =
        safe(() => {
            let safe_eight : Safe<number, string> = ge5(8);
            let eight      : number               = unsafe<number, string>(safe_eight);
            return eight;
        });
    console.log(foo);
    //// fails to typecheck
    //let bar =
    //    safe(() => {
    //        let safe_eight : Safe<number, string> = ge5(8);
    //        let eight      : number               = unsafe<number, number>(safe_eight);
    //        return eight;
    //    });
    //console.log(bar);
    // typechecks
    let baz =
        safe(() => {
            let ok_eight : Safe<number, string> = ok(8);
            let eight    : number               = unsafe<number, string>(ok_eight);
            return eight;
        });
    console.log(baz);
    // typechecks
    let quux =
        safe(() => {
            let ok_eight : Safe<number, string> = ok(8);
            let eight    : number               = unsafe<number, number>(ok_eight);
            return eight;
        });
    console.log(baz);
}
    //let bar =
    //    safe(() => {
    //        let eight      : number = unsafe<number, string>(ge5(8));
    //        let minusThree : number = sub11(eight);
    //        let x          : number = unsafe<number, string>(ge5(minusThree));
    //        return x;
    //    });
    //console.log(bar);

main();
