/**
 * Miscellany
 *
 * @module
 */



//=============================================================================
// IMPORTS
//=============================================================================

import type {
    ERROR_TYPE_TimeoutError
} from './errcode.js';



//=============================================================================
// EXPORTS
//=============================================================================

export {
    VanillaeError
};

export type {
    TimeoutError
};



//=============================================================================
// CODE
//=============================================================================

/**
 * Vanillae errors are guaranteed to have a unique `code`
 */
interface VanillaeError
{
    code    : number;
    message : string;
};



/**
 * Error generated on Timeout
 */
type TimeoutError
    = {code    : ERROR_TYPE_TimeoutError,
       message : string};
