/**
 * # SafeM monad
 *
 * Remember that types are fake. Also do not take the words "safe" or "monad"
 * at face value, the concept here is only meant to be loosely analogous. If
 * you don't know what a monad is, it's basically like a burrito for your code.
 *
 * ## Motivation
 *
 * We are making calls to the wallet which may or may not work.  We are putting
 * events into an event queue and hoping to get events back.  Sometimes that
 * doesn't work.
 *
 * There are generally two reasons that it doesn't work:
 *
 * 1.  there was a timeout (say, the wallet extension doesn't exist)
 * 2.  the wallet sent back an error result instead of the requested data (say,
 *     the user rejected a transaction)
 *
 * In neither case do we want there to be a crash. We want crashes when
 * something insane happens (the "negative" class of errors). For the strictly
 * enumerated positive class of errors that we know are likely to occur
 * (e.g. timeouts, RPC errors), we want a different mechanism.
 *
 * Imagine if you were writing an HTTP server. You do not want to generate
 * crashes on things that happen all the time, like 404 errors. Crashes
 * are meant to correspond to genuinely exceptional behavior (they're called
 * "exceptions" for a reason!), like a hardware failure or disk corruption.
 *
 * In our terminology, crashes handle the negative class of errors.  We want a
 * different idiom to correspond to the positive class of errors.
 *
 * The kernel of this idea probably comes from me reading Nassim Taleb. Taleb
 * uses the term "Black Swan" to mean roughly what I am calling "negative
 * errors".  One idea Taleb stresses repeatedly is that the correct approach to
 * handling positive errors has almost nothing in common with the correct
 * approach to handling negative errors.  Taleb's writing contains no shortage
 * of important, useful, and counterintuitive ideas.  He's also fun to read.
 *
 * Let me emphasize again the idea to not take "safe" at face value: we are
 * _only_ providing "safety" against the explicitly enumerated positive
 * category (shouldn't say "class" because OOP, oops... ha, oops, there's a
 * joke hidden there...) of errors. For the negative category of errors,
 * we are still using crashes as our tool to handle them.
 *
 * For another example, this library is written in TypeScript. In other words,
 * a strictly defined positive category of errors is defined (type mismatches),
 * and `tsc` provides some level of "safety" against that positive class. tsc
 * does not catch every single possible bug you might write into your program
 * (the negative category), only the positive category.
 *
 * Let us examine the approaches to this problem taken by two other languages:
 * Erlang and Haskell.  We will see that Haskell and Erlang have conceptually
 * identical approaches to handling positive errors, and differ in how they
 * handle negative errors.
 *
 * Haskell has a very interesting way of thinking about positive errors, which
 * is what this file is trying to imitate.
 *
 * I mention Erlang because TypeScript's type system is almost identical to
 * Erlang's.
 *
 * ## Erlang's Approach to Negative Errors
 *
 * Erlang is a concurrency-oriented language, in the same way that say Java is
 * an object-oriented language.  Processes play the same role in Erlang that
 * objects play in an object-oriented language.
 *
 * Erlang's approach to negative errors can be summarized as "let it crash".
 * This works in a highly concurrent setting, because if a single process
 * crashes, it is not really a big deal.  There is no shared memory across
 * processes, or global data, so it is not possible for corrupted state in a
 * single process to leak out and corrupt other processes.  Thus Erlang
 * _actually_ has encapsulation, which every object-oriented language _claims_
 * to have, but none actually do.
 *
 * Put about 15 asterisks on that paragraph above, but that's the basic idea.
 *
 * We also must mention the meaning of `=` in Erlang.  Within the scope of a
 * function, variables have single assignment:
 *
 * ```erlang
 * 1> X = 5.
 * 5
 * 2> X = 3.
 * ** exception error: no match of right hand side value 3
 * ```
 *
 * `=` in Erlang is _both_ assertion __and__ assignment.  If the value on the
 * left does not match the value on the right, the process crashes.  The effect
 * when programming is that you "program against the positive case" so that
 * your code is "correct by definition": _if a thread is still running, then
 * you can safely assume it is working correctly_.  A common idiom is to put
 * inline validations in the body of functions. This is an example from some
 * math code, do not concern yourself with what it does:
 *
 * ```erlang
 * % There is a data structure called a "word", which is a wrapper type around
 * % a set. We are defining multiplication on words, in terms of what it does
 * % to the underlying sets. L and R are sets
 * times({w, L}, {w, R}) ->
 *     % LR is the union of the two sets
 *     LR = sets:union(L, R),
 *     % we form the new "word"
 *     Word = {w, LR},
 *     % We assert that this word is valid; if it is not, the program crashes
 *     % right here. The error message prints out the callstack and exactly
 *     % what data caused the crash.
 *     true = is_valid_word(Word),
 *     % We then return the word, now that we know it is valid
 *     Word.
 * ```
 *
 * The idiom here is to *assume* and *assert* that everything worked correctly,
 * and crash if it didn't.
 *
 * Erlang has a very elegant and unique approach to handling negative errors.
 * Unfortunately for us, this approach leans very heavily on specific features
 * of the Erlang runtime which are not present in JavaScript.  However, we can
 * still learn from it, and draw lessons that are applicable to our situation.
 *
 * Specifically, we draw the lessons that
 *
 * 1.  We should not attempt to recover from negative errors.
 * 2.  Most important point: __we never proceed with bad data.__
 *
 * ## Erlang's Approach to Positive Errors
 *
 * This example is taken from an HTTP server written in Erlang. This function
 * is called when the browser requests a static resource, such as a stylesheet.
 *
 * ```erlang
 * fetch_static(State, ID) ->
 *     % we look in the cache and see if this static resource is in the cache
 *     case ah_html_cache:lookup(static, ID, none) of
 *         % if it is, then send back the data
 *         {ok, {Data, Type}} ->
 *             R = #response{code = 200, slogan = "OK", type = {data, Type}, body = Data},
 *             {State, R};
 *         % if it is not in the cache, need to proceed to a deeper
 *         % investigation (querying the filesystem, returning a 404 if the
 *         % resource is not found, updating the cache if it is)
 *         error ->
 *             fetch_static2(State, ID)
 *     end.
 * ```
 *
 * The point being, we don't want to crash our connection with the web browser
 * just because of a cache miss.  A cache miss is a positive error.  It would
 * be a mistake to do this:
 *
 * ```erlang
 * fetch_static(State, ID) ->
 *     % here we are asserting that the cache lookup was successful, and
 *     % crashing in the event of a cache miss
 *     {ok, {Data, Type}} = ah_html_cache:lookup(static, ID, none),
 *     R = #response{code = 200, slogan = "OK", type = {data, Type}, body = Data},
 *     {State, R}.
 * ```
 *
 * We cannot directly port Erlang's `case`-splitting approach into JavaScript,
 * because JavaScript does not have pattern matching and therefore no real
 * `case` statement.
 *
 * So we have to try something a little bit different.
 *
 * ## Haskell's (and Sidekick's) Approach to Handling Positive Errors
 *
 * Like I said, Haskell and Erlang have conceptually identical approaches to
 * handling positive errors.  However, Haskell has an interesting idiom (called
 * the "monad") to add to the conversation.
 *
 * It's very hard to fake the case-splitting approach to positive error
 * handling.  It just doesn't fit in JavaScript.  However... surprisingly... we
 * _can_ fake the monad idiom in a somewhat elegant way.  Well, elegant by
 * JavaScript standards.
 *
 * Let's consider the following Haskell code
 *
 * ```haskell
 * module SafeM where
 *
 * -------------------------------------------------------------------------------
 * -- Type
 * -------------------------------------------------------------------------------
 *
 * data Safe ok_type err_type
 *     = Ok ok_type
 *     | Errs [err_type]
 *     deriving (Eq, Show)
 * ```
 *
 * This is conceptually all we have below: a generic type (i.e. function at the
 * level of types) which has a "success version" and an "error version".
 *
 * ```typescript
 * type Safe<ok_t, err_t> =
 *     | Ok<ok_t>
 *     | Errs<err_t>;
 *
 * type Ok<ok_t> =
 *     {ok     : true
 *      result : ok_t};
 *
 * type Errs<err_t> =
 *     {ok   : false
 *      errs : Array<err_t>};
 * ```
 *
 * They work differently in very meaningful ways, but the basic idea is the
 * same.
 *
 * Let's see a trivial example of how we might want to use this:
 *
 * ```haskell
 * -- |Example: asserts that the integer is greater than or equal to 5
 * ge5 :: Int -> Safe Int String
 * ge5 n =
 *     if n >= 5
 *     then
 *         Ok n
 *     else
 *         let msg = concat ["error: ", show n, " is less than 5"]
 *          in Errs [msg]
 * ```
 *
 * ```haskell
 * ghci >> :l SafeM
 * [1 of 1] Compiling SafeM            ( SafeM.hs, interpreted )
 * Ok, modules loaded: SafeM.
 * ghci >> ge5 8
 * Ok 8
 * it :: Safe Int String
 * ghci >> ge5 5
 * Ok 5
 * it :: Safe Int String
 * ghci >> ge5 2
 * Errs ["error: 2 is less than 5"]
 * it :: Safe Int String
 * ```
 *
 * In our terminology: this `ge5` function explicitly guards against a positive
 * "error", meaning it only "succeeds" if the input argument is greater than or
 * equal to 5.
 *
 * ### Functorial properties: applying safety-wrapped values to pure functions
 *
 * The next thing you might want to do is apply a pure map over a
 * safety-wrapped value:
 *
 * ```haskell
 * -------------------------------------------------------------------------------
 * -- Mapping (Functorial properties)
 * -------------------------------------------------------------------------------
 *
 * -- |Given a pure map, and a safety-wrapped value, apply the pure map to the
 * -- safety-wrapped value
 * safe_map :: (ok_t1 -> ok_t2)     -- ^pure map
 *          -> Safe ok_t1 err_t     -- ^wrapped input value
 *          -> Safe ok_t2 err_t     -- ^output value
 * safe_map fun safe_value =
 *     case safe_value of
 *         Ok value ->
 *             Ok (fun value)
 *         Errs errs ->
 *             Errs errs
 * ```
 *
 * ```haskell
 * ghci >> safe_map (\x -> x * 3) (Ok 3)
 * Ok 9
 * it :: Num ok_t2 => Safe ok_t2 err_t
 * ghci >> safe_map (\x -> x * 3) (Errs ["something went wrong"])
 * Errs ["something went wrong"]
 * it :: (Data.String.IsString err_t, Num ok_t2) => Safe ok_t2 err_t
 * ghci >> let upcase_string = \str -> map toUpper str in safe_map upcase_string (Ok "hello world")
 * Ok "HELLO WORLD"
 * it :: Safe [Char] err_t
 * ghci >> let upcase_string = \str -> map toUpper str in safe_map upcase_string (Errs ["hello world"])
 * Errs ["hello world"]
 * it :: Data.String.IsString err_t => Safe [Char] err_t
 * ```
 *
 * We can see that this has the following behavior:
 *
 * 1.  If the safety-wrapped input to the function was successful, it unwraps
 *     the value, applies the function to the unwrapped value, and re-wraps the
 *     result.
 * 2.  If the safety-wrapped input to the function was a failure, it simply
 *     propagates the failure forward and doesn't use the function at all
 *
 *     __This is the most important idea:__ this "monad" idiom (which I'm
 *     getting to in a moment) allows us to propagate positive errors forward
 *     in a functional and _composable_ way so that—most important point—__we
 *     never proceed with bad data__ and at the same time _we capture positive
 *     errors without resorting to crashes._
 *
 * Again, the relevant Haskell code
 *
 * ```haskell
 *     case safe_value of
 *         Ok value ->
 *             Ok (fun value)
 *         Errs errs ->
 *             Errs errs
 * ```
 *
 * In SafeM, we have this instead:
 *
 * ```typescript
 * function
 * smap
 *     <ok_t1, ok_t2, err_t>
 *     (fun   : (_arg0: ok_t1) => ok_t2,
 *      input : Safe<ok_t1, err_t>)
 *     : Safe<ok_t2, err_t>
 * {
 *     // if it's a success, do the map
 *     if (input.ok)
 *     {
 *         let new_result = fun(input.result);
 *         return ok(new_result);
 *     }
 *     // if it's an error, leave it
 *     else
 *     {
 *         return input;
 *     }
 * }
 * ```
 *
 * ### Monadic properties: composability
 *
 * Let's talk a bit about composability.
 *
 * Let's suppose we have two things:
 *
 * 1.  Our safety filter `ge5` that checks to make sure the input number is
 *     greater than or equal to `5`
 *
 *     ```haskell
 *     ge5 :: Int -> Safe Int String
 *     ge5 n =
 *         if n >= 5
 *         then
 *             Ok n
 *         else
 *             let msg = concat ["error: ", show n, " is less than 5"]
 *              in Errs [msg]
 *     ```
 *
 * 2.  Our pure integer operation that subtracts `11` from its input
 *
 *     ```haskell
 *     -- |Subtracts 11
 *     sub11 :: Int -> Int
 *     sub11 n =
 *         n - 11
 *     ```
 *
 * Using just the functorial properties, we can easily put our `ge5` filter
 * _before_ the `sub11` operation in the processing chain:
 *
 * ```haskell
 * ghci >> safe_map sub11 (ge5 8)
 * Ok (-3)
 * it :: Safe Int String
 * ghci >> safe_map sub11 (ge5 2)
 * Errs ["error: 2 is less than 5"]
 * it :: Safe Int String
 * ```
 *
 * However, if we want to subtract 11 _first_ and _then_ filter to see if the
 * result is bigger than or equal to `5`, we currently don't have any way to do
 * that. If we look at the types again it should be clear why:
 *
 * ```haskell
 * ghci >> :t ge5
 * ge5 :: Int -> Safe Int String
 * ghci >> :t safe_map sub11
 * safe_map sub11 :: Safe Int err_t -> Safe Int err_t
 * ghci >> :t safe_map sub11 . ge5
 * safe_map sub11 . ge5 :: Int -> Safe Int String
 * ```
 *
 * Basically, `ge5` lifts us up into the safe context from a pure context, and
 * then `safe_map sub11` operates entirely within the safe context.
 *
 * What we want to be able to do is to temporarily pull the result of `safe_map
 * sub11` out of the safe context, plug it into `ge5`, and then have a result
 * back in the safe context (or at least pretend we're doing this).  And again,
 * we want to do this in a way that propagates our positive errors forward, so
 * that—most important point—__we never proceed with bad data.__
 *
 * This is where the "monad" idiom comes in. The functorial primitive is `map`.
 * The monadic primitive is `bind`.
 *
 * ```haskell
 * -------------------------------------------------------------------------------
 * -- Binding (monadic properties)
 * -------------------------------------------------------------------------------
 *
 * safe_bind :: (ok_t1 -> Safe ok_t2 err_t)
 *           -> Safe ok_t1 err_t
 *           -> Safe ok_t2 err_t
 * safe_bind fun safe_val =
 *     case safe_val of
 *         Ok val ->
 *             fun val
 *         Errs errs ->
 *             Errs errs
 * ```
 *
 * And, alas, we can now do our compositions properly:
 *
 * ```haskell
 * ghci >> :t safe_bind ge5 . safe_map sub11
 * safe_bind ge5 . safe_map sub11
 *   :: Safe Int String -> Safe Int String
 * ghci >> :t safe_bind ge5 . safe_map sub11 . ge5
 * safe_bind ge5 . safe_map sub11 . ge5 :: Int -> Safe Int String
 * ghci >> (safe_bind ge5 . safe_map sub11 . ge5) 8
 * Errs ["error: -3 is less than 5"]
 * ghci >> (safe_map sub11 . ge5) 8
 * Ok (-3)
 * it :: Safe Int String
 * ghci >> (safe_bind ge5 . safe_map sub11 . ge5) 25
 * Ok 14
 * it :: Safe Int String
 * ghci >> (safe_bind ge5 . safe_map sub11 . ge5) (5 + 11)
 * Ok 5
 * it :: Safe Int String
 * ghci >> (safe_bind ge5 . safe_map sub11 . ge5) (-2)
 * Errs ["error: -2 is less than 5"]
 * it :: Safe Int String
 * ```
 *
 * ### Summary
 *
 * The monad idiom allows us to propagate positive errors forward in our
 * program in a "safe" way so that—most important point, say it out loud—__we
 * never proceed with bad data.__
 *
 * There's a few more primitives here that correspond to other situations that
 * come up in practice which I didn't mention. But, the most important ideas
 * that are toughest to wrap your head around are out of the way.
 *
 * There's a number of subtle differences between Haskell monads and Sidekick
 * monads, stemming from differences between the two languages.  Proper monads
 * in Haskell are a much more general idea than what I explained above, and the
 * implementation details differ in subtle but significant ways. Moreover,
 * Haskell has some syntax sugar (the `do` block) that allows the programmer to
 * forget about the specific `bind` mechanics going on.  It's very similar to
 * the way `async`/`await` works in JavaScript. Actually... `Promise`s *are* a
 * proper monad, and I believe the `async`/`await` idiom originates in
 * Haskell.  I heard that somewhere, and the idea reeks too much of Haskell to
 * not originate in Haskell.  Our `bind` idiom is isomorphic to the
 * `.then((value) => {...})` idiom in JavaScript.  So roughly
 *
 * ```typescript
 * async function foo(inputA : TypeA) : Promise<TypeB> {
 *     ...
 * }
 *
 * async function bar(inputB : TypeB) : Promise<TypeC> {
 *     ...
 * }
 *
 * async function baz(inputC : TypeC) : Promise<TypeD> {
 *     ...
 * }
 *
 * // two ways through:
 * // 1: using .then() which mirrors the bind idiom:
 * async function quux_bind(inputA : TypeA) : Promise<TypeD> {
 *     let result =
 *          foo(inputA)
 *              .then((inputB) => {return bar(inputB)})
 *              .then((inputC) => {return baz(inputC)});
 *     return result;
 * }
 *
 * // 2: using await, which mirrors the do idiom
 * async function quux_do(inputA : TypeA) : Promise<TypeD> {
 *     let inputB = await foo(inputA);
 *     let inputC = await bar(inputB);
 *     return baz(inputC);
 * }
 * ```
 *
 * In Haskell you would do this (roughly):
 *
 * ```haskell
 * foo :: TypeA -> Promise TypeB
 * foo inputA = ...
 *
 * bar :: TypeB -> Promise TypeC
 * bar inputB = ...
 *
 * baz :: TypeC -> Promise TypeD
 * baz inputC = ...
 *
 * quuxBind :: TypeA -> Promise TypeD
 * quuxBind inputA =
 *     -- more in line with the code I wrote
 *     (bind baz . bind bar . foo) inputA
 *
 * quuxBind' :: TypeA -> Promise TypeD
 * quuxBind' inputA =
 *     -- more like what you would actually see in Haskell
 *     foo inputA
 *         >>= \inputB -> bar inputB
 *         >>= \inputC -> baz inputC
 *
 * quuxDo :: TypeA -> Promise TypeD
 * quuxDo inputA =
 *     -- the `do` syntax sugar in Haskell, which mirrors the `await` idiom
 *     do inputB  <- foo inputA
 *        inputC  <- bar inputB
 *        resultD <- baz inputC
 *        return resultD
 * ```
 *
 * Explaining Haskell monads is out of scope of this document (blog post
 * maybe...).  Moreover, Haskell's type system is considerably more
 * sophisticated than TypeScript's. Proper Haskell monads are simply not
 * possible in TypeScript due to the lack of higher-kinded types.  Well maybe
 * it's possible with some weird oopy abstract class object voodoo, but I
 * really don't want to do that.
 *
 * TypeScript's type system is a lot more flexible than Haskell's. For
 * instance, Haskell doesn't let us construct "anonymous types". Notice the
 * difference between the two function signatures
 *
 * ```typescript
 * function
 * bind
 *     <ok_t1, ok_t2, err_t1, err_t2>
 *     (fun   : (_arg0: ok_t1) => Safe<ok_t2, err_t2>,
 *      input : Safe<ok_t1, err_t1>)
 *     : Safe<ok_t2, err_t1 | err_t2>
 * ```
 *
 * ```haskell
 * safe_bind :: (ok_t1 -> Safe ok_t2 err_t)
 *           -> Safe ok_t1 err_t
 *           -> Safe ok_t2 err_t
 * ```
 *
 * In Haskell, we have the constraint that the input and output error types
 * `err_t` must be the same. In TypeScript, we can simply combine them with
 * `err_t1 | err_t2`
 *
 * ## Bottom line
 *
 * What matters is we have an idiom that lets us handle positive errors in a
 * composable and functional way, so that—most important point—__we never
 * proceed with bad data.__
 *
 * @module
 */

// TODO(pharpend): maybe want to redo this with spread/rest syntax...
// i think everything from this point forward will follow from usage in
// practice



//=============================================================================
// IMPORTS
//=============================================================================

import type {
    VanillaeError
} from './etc.js';



//=============================================================================
// EXPORTS
//=============================================================================

export type {
    ErrPlus,
    Ok,
    Err
};

export {
    ok,
    err,
    unsafe
    // deleted safe because it breaks everything, i.p. it breaks all the
    // assumptions about error structure, and there's no way to unbreak that in
    // a sane way.
};



//=============================================================================
// CODE
//=============================================================================

//-----------------------------------------------------------------------------
// Type definitions
//-----------------------------------------------------------------------------

type ErrPlus
    <ok_t  extends any,
     err_t extends VanillaeError>
    = Ok  <ok_t>
    | Err <err_t>;



type Ok
    <ok_t extends any>
    = {ok     : true
       result : ok_t};



type Err
    <err_t extends VanillaeError>
    = {ok  : false
       err : err_t};



//-----------------------------------------------------------------------------
// Constructors
//-----------------------------------------------------------------------------

/**
 * Construct an `Ok` value
 *
 * Rough analog of `pure` or `return` in Haskell
 */
function
ok
    <ok_t extends any>
    (value : ok_t)
    : Ok<ok_t>
{
    return {ok     : true,
            result : value};
}


/**
 * Construct an `Err` value from an error.
 *
 * Rough analog of `fail` in Haskell.
 */
function
err
    <err_t extends VanillaeError>
    (err : err_t)
    : Err<err_t>
{
    return {ok: false, err: err};
}



//----------------------------------------------------------------------------
// unsafe/safe idiom
//----------------------------------------------------------------------------


/**
 * throws an exception if the argument is an error, otherwise returns the
 * result
 */
function
unsafe
    <ok_t  extends any,
     err_t extends VanillaeError>
    (x : ErrPlus<ok_t, err_t>)
    : ok_t
{
    if (x.ok)
        return x.result;
    else
        throw x.err;
}


///**
// * try/catch
// *
// * This `unknown` thing is the best we can honestly do
// */
//function
//safe
//    <ok_t extends any>
//    (fun : () => ok_t)
//    : Safe<ok_t,
//           unknown>
//{
//    try
//    {
//        let result = fun();
//        return ok(result);
//    }
//    catch (e)
//    {
//        return err(e);
//    }
//}
