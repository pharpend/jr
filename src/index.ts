import {
    MsgQ
} from './raseev.js';


async function
main
    ()
{
    let my_tgt = new EventTarget();

    let my_sk = new MsgQ;
    my_sk.listen(my_tgt);

    my_tgt.dispatchEvent(new MessageEvent('message', {data: 'hello'}));
}

main();
