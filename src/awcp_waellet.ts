/**
 * Describes how a waellet should behave _from the aepp's perspective_.
 *
 * @module
 */

import * as awcp    from './awcp.js';
import * as errplus from './errplus.js';
import * as etc     from './etc.js';

export {
    // waellet interface
    AWCP_Waellet
};



interface AWCP_Waellet
{
    connection_announcePresence : (                                                 timeout_ms : number) => Promise<errplus.ErrPlus<awcp.Params_W2A_connection_announcePresence,                 etc.TimeoutError>>;
    connection_open             : (req_params : awcp.Params_A2W_connection_open  ,  timeout_ms : number) => Promise<errplus.ErrPlus<awcp.Result_W2A_connection_open,             awcp.RpcError | etc.TimeoutError>>;
    address_subscribe           : (req_params : awcp.Params_A2W_address_subscribe,  timeout_ms : number) => Promise<errplus.ErrPlus<awcp.Result_W2A_address_subscribe,           awcp.RpcError | etc.TimeoutError>>;
    tx_sign_yesprop             : (req_params : awcp.Params_A2W_tx_sign_yesprop  ,  timeout_ms : number) => Promise<errplus.ErrPlus<awcp.Result_W2A_tx_sign_yesprop,             awcp.RpcError | etc.TimeoutError>>;
    tx_sign_noprop              : (req_params : awcp.Params_A2W_tx_sign_noprop   ,  timeout_ms : number) => Promise<errplus.ErrPlus<awcp.Result_W2A_tx_sign_noprop,              awcp.RpcError | etc.TimeoutError>>;
}
