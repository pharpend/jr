/**
 * Logger
 *
 * @module
 */


type LogLevel
    = 'debug'
    | 'log'
    | 'warning'
    | 'error'
    ;

class Logger
{
    log
        (log_level : LogLevel,
         message   : string,
         ...params : any)
     {
        console.log(`${log_level}: ${message}, ${params}`);
     }
}
