/**
 * Message Queue (block on raseev) implementation
 *
 * @module
 */



//=============================================================================
// IMPORTS
//=============================================================================

import {
    Params_W2A_connection_announcePresence,
    RpcResp_Any
} from './awcp.js';



//=============================================================================
// EXPORTS
//=============================================================================

export {
    MsgQ
};



//=============================================================================
// CODE
//=============================================================================

/**
 * These are the types of messages that we are expecting
 *
 * @internal
 */
type QMsg
    = Params_W2A_connection_announcePresence
    | RpcResp_Any;



///**
// * This function exist outside the class context because JS has weird
// * evaluation rules
// */
//function
//listener
//    (msgq : MsgQ,
//     msg  : MessageEvent<QMsg>)
//    : void
//{
//    msgq.handle(msg.data);
//}



class MsgQ
{
    /**
     * the queue for "raseev by id"
     */
    call_result_queue : Map<number, RpcResp_Any>                            = new Map()     ;

    /**
     * The queue for "connection.announcePresence"
     *
     * is either nonexistent or contains a single message
     */
    cap_queue         : undefined | Params_W2A_connection_announcePresence  = undefined ;

    /**
     * The listener function because stupid js pointer hack
     */
    ls                : (msg : MessageEvent<QMsg>) => void                  ;


    constructor
        ()
    {
        // stupid js pointer hack because `this` is lazy evaluated outside the
        // class context
        const this_ptr = this;
        this.ls =
            function(message : MessageEvent<QMsg>)
            {
                //console.log(message);
                this_ptr.handle(message);
            };
    }


    /**
     * Listen to messages from a given `EventTarget`
     */
    listen
        (tgt : EventTarget)
    {
        tgt.addEventListener('message', this.ls);
    }



    /**
     * Ignore messages from a given `EventTarget`
     */
    ignore
        (tgt : EventTarget)
    {
        tgt.removeEventListener('message', this.ls);
    }



    handle
        (msg : MessageEvent<QMsg>)
        : void
    {
        console.log('penis');
        //let data : QMsg = msg.data;
        //if (is_cap(data))
        //{
        //    this.handle_cap(data);
        //}
        //else if (is_call_result(data))
        //{
        //    this.handle_call_result(data);
        //}
        //else
        //{
        //    console.log('warning', 'message queue received unexpected message', msg);
        //}
    }
//
//
//
//      handle_cast
//          (msg : RpcCast<string, object>)
//      {
//          let method = msg.method;
//          let params = msg.params;
//          this.maybe_add_to_cast_queue(method, params);
//      }
//
//
//
//    handle_call_result
//        (msg : RpcResp<number, string, any, any>)
//    {
//        let key            = msg.id;
//        let errplus_result = rpcresp_to_errplus(msg);
//        this.add_to_call_result_queue(key, errplus_result);
//    }

}
