// outside in:
//
// 

/** waellet-to-aepp notification (only instance: "connection.announcePresence" */
export type W2A_Noti<method_s, params_t> =
    {type : "to_aepp",
     data : RpcNoti<method_s, params_t>};


/** aepp-to-waellet request (e.g. "sign this") */
export type A2W_Requ<method_s, params_t> =
    {type : "to_waellet",
     data : RpcRequ<method_s, params_t>};

/** aepp-to-waellet request (e.g. "sign this") */
export type W2A_Resp<method_s, result_t> =
    {type : "to_aepp",
     data : RpcResp<method_s, result_t>};


export type RpcRequ<method_s, params_t> =
    {jsonrpc : "2.0",
     id      : number,
     method  : method_s,
     params  : params_t};

export type RpcResp<method_s, result_t> =
    {jsonrpc : "2.0",
     id      : number,
     method  : method_s,
     result  : result_t};

enum RpcErrorCode
{
    RpcInvalidTransactionError  : number =      2,
    RpcBroadcastError           : number =      3,
    RpcRejectedByUserError      : number =      4,
    RpcUnsupportedProtocolError : number =      5,
    RpcConnectionDenyError      : number =      9,
    RpcNotAuthorizeError        : number =     10,
    RpcPermissionDenyError      : number =     11,
    RpcInternalError            : number =     12,
    RpcMethodNotFoundError      : number = -32601 
}

export type RpcError =
    {code    : RpcErrorCode,
     message : string,
     data?   : any};




export type W2A_params_connection_announcePresence =
    {id     : string,
     name   : string,
     origin : string,
     type   : "window" | "extension"};



export type AWCP_W2A_requ_connection_announcePresence =
    Window_W2A_Requ<"connection.announcePresence",
                    AWCP_W2A_params_connection_announcePresence>;




//-------------------------------------------------------------------
// connection.open
//-------------------------------------------------------------------

/**
 * `connection.open` aepp-to-waellet message parameters
 *
 * NOTE(dak): `networkId` appears by experimentation to be optional.
 * This may be a source of bugs in the future if it turns out to not
 * be optional
 */
export type AWCP_A2W_params_connection_open =
    {name       : string,
     version    : number,
     networkId? : string};



/**
 * `connection.open` wallet-to-aepp result
 * happens to be the same data we get spammed with for
 * `connection.announcePresence`
 */
export type AWCP_W2A_result_connection_open =
    AWCP_W2A_params_connection_announcePresence;



export type AWCP_A2W_requ_connection_open =
    Window_A2W_Requ<"connection.open",
                    AWCP_A2W_params_connection_open>;



export type AWCP_W2A_resp_connection_open =
    Window_W2A_Resp<"connection.open",
                    AWCP_W2A_result_connection_open>;



//-------------------------------------------------------------------
// address.subscribe
//-------------------------------------------------------------------

/**
 * `address.subscribe` messages
 */
export type AWCP_A2W_params_address_subscribe =
    {type  : "subscribe",
     value : "connected"};



export type AWCP_W2A_result_address_subscribe =
    {subscription : Array<string>,
     address      : {current   : object,
                     connected : object}};



export type AWCP_A2W_requ_address_subscribe =
    Window_A2W_Requ<"address.subscribe",
                    AWCP_A2W_params_address_subscribe>;



export type AWCP_W2A_resp_address_subscribe =
    Window_W2A_Resp<"address.subscribe",
                    AWCP_W2A_result_address_subscribe>;



//-------------------------------------------------------------------
// transaction.sign (propagate)
//
// Two different types: semantically, whether or not the wallet
// should propagate the message, or just return the signed
// transaction
//
// This returnSigned parameter apparently controls whether Superhero
// attempts to send the transaction into the blockchain (returnSigned
// = false), or simply signs the transaction and sends it back to you
// (returnSigned = true)
//-------------------------------------------------------------------


export type AWCP_A2W_params_transaction_sign_yes_propagate =
    {tx           : string,
     returnSigned : false,
     networkId    : string};



/**
 * this is when returnSigned: false
 * the wallet attempts to propagate the transaction
 */
export type AWCP_W2A_result_transaction_sign_yes_propagate =
    {transactionHash: {blockHash   : string,
                       blockHeight : number,
                       hash        : string,
                       signatures  : Array<string>,
                       tx          : {amount      : number,
                                      fee         : number,
                                      nonce       : number,
                                      payload     : string,
                                      recipientId : string,
                                      senderId    : string,
                                      type        : string,
                                      version     : number},
                       rawTx       : string}};



export type AWCP_A2W_requ_transaction_sign_yes_propagate =
    Window_A2W_Requ<"transaction.sign",
                    AWCP_A2W_params_transaction_sign_yes_propagate>;



export type AWCP_W2A_resp_transaction_sign_yes_propagate =
    Window_W2A_Resp<"transaction.sign",
                     AWCP_W2A_result_transaction_sign_yes_propagate>;



export type AWCP_A2W_params_transaction_sign_no_propagate =
    {tx           : string,
     returnSigned : true,
     networkId    : string};



export type AWCP_W2A_result_transaction_sign_no_propagate =
    {signedTransaction: string};



export type AWCP_A2W_requ_transaction_sign_no_propagate =
    Window_A2W_Requ<"transaction.sign",
                    AWCP_A2W_params_transaction_sign_no_propagate>;



export type AWCP_W2A_resp_transaction_sign_no_propagate =
    Window_W2A_Resp<"transaction.sign",
                     AWCP_W2A_result_transaction_sign_no_propagate>;


export interface AWCP_Waellet
{
    connection_announcePresence    : (                                                   timeout_ms : number ) => Promise< AWCP_W2A_requ_connection_announcePresence    >;
    connection_open                : ( _: AWCP_A2W_requ_connection_open                , timeout_ms : number ) => Promise< AWCP_W2A_resp_connection_open                >;
    address_subscribe              : ( _: AWCP_A2W_requ_address_subscribe              , timeout_ms : number ) => Promise< AWCP_W2A_resp_address_subscribe              >;
    transaction_sign_yes_propagate : ( _: AWCP_A2W_requ_transaction_sign_yes_propagate , timeout_ms : number ) => Promise< AWCP_W2A_resp_transaction_sign_yes_propagate >;
    transaction_sign_no_propagate  : ( _: AWCP_A2W_requ_transaction_sign_no_propagate  , timeout_ms : number ) => Promise< AWCP_W2A_resp_transaction_sign_no_propagate  >;
};
