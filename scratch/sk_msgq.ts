type WindowMsg_W2A =
    {type : "to_aepp",
     data : {method: string}};


function
listener
    (msgq : MsgQ,
     evt  : MessageEvent<WindowMsg_W2A>)
    : void
{
    let msg             = evt.data;
    let event_is_for_us = ("to_aepp" === msg.type)

    if (event_is_for_us)
        msgq.handle(msg);
    else
        return;
}


/**
 * Most of the time we want to raseev by ID, only on
 * connection.announcePresence do we want to raseev by method
 *
 */
class MsgQ
{
    queue     : object                                    = {}    ;
    listening : boolean                                   = false ;
    ls        : (_ : MessageEvent<WindowMsg_W2A>) => void         ;


    constructor() {
        const pointer_to_this = this;
        this.ls =
            function (evt) {
                listener(pointer_to_this, evt);
            };
    }


    listen(x: EventRouter): void {
        x.addEventListener('message', this.ls);
        this.listening = true;
    }


    ignore(x: EventRouter): void {
        x.removeEventListener('message', this.ls);
        this.listening = false;
    }


    async rcv_by_method<return_type>(method: string, timeout_ms: number): Promise<Safe<return_type, SkError_Timeout | SkError_MsgQNotListening>> {
        let this_ptr = this;
        let queue_has_messages_we_want =
            function() {
                return this_ptr.queue_has_message_with_method(method);
            };

        let _0 : Safe<true, SkError_Timeout> =
            await eicifebulrt(
                // sanity check:
                (this_ptr.listening || queue_has_messages_we_want()),
                // sanity failure message:
                "message queue is not listening and there are no matching messages in the queue",
                // lazily evaluated unblocking condition:
                queue_has_messages_we_want,
                // timeout (milliseconds)
                timeout_ms,
                // timeout failure message:
                `timeout failure: message queue has no messages with method ${method} after ${timeout_ms} milliseconds`
            );

        let msg = this.queue[method].pop();

        if(msg.data.error)
            throw new Error(msg.data.error.message);
        else
            return msg;
    }


    handle(msg: WindowMsg_W2A) : void {
        this.add_to_queue(msg);
    }



    add_to_queue(msg: WindowMsg_W2A) : void {
        let method = msg.data.method;
        let key_exists = !!(this.queue[method]);

        if (method === "connection.announcePresence"
            && this.queue_has_message_with_method("connection.announcePresence")) {
            return;
        }
        else if (key_exists) {
            (this.queue[method]).push(msg);
        }
        else {
            let arr = [msg];
            this.queue[method] = arr;
        }
    }



    queue_has_message_with_method(method: string): boolean {
        let key_exists = !!(this.queue[method]);
        let key_dne    = !key_exists;
        if (key_dne)
            return false;
        else
        {
            let messages_in_the_queue_that_are_of_the_given_method =
                this.queue[method];
            let said_messages_array_is_empty =
                (0 === messages_in_the_queue_that_are_of_the_given_method.length);
            let there_are_messages = !said_messages_array_is_empty;
            return there_are_messages;
        }
    }
}

function start(): MsgQ {
    return new MsgQ;
}

export type {
    WindowMsg_W2A,
    MsgQ
}
export {
    start
}
